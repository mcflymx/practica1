#include <stdio.h>
#include <stdlib.h>
#include "tiempo.h"


main( int argc , char **argv ){
                //******************************************************************            
                //Iniciar el conteo del tiempo para las evaluaciones de rendimiento
                //******************************************************************            
                double utime0, stime0, wtime0,utime1, stime1, wtime1;
                //******************************************************************
                
                uswtime(&utime0, &stime0, &wtime0);
                
                int *numeros = NULL;
                int i,n;
                int numero;
                FILE *salida;
                
                if(argc != 3){                  
                  fprintf(stderr, "Forma de uso:%s 10 archivo.txt \n",argv[0]);
                  exit(-1);
                }
                
                salida = fopen(argv[2], "w");
                
                if(salida == NULL){
                  perror("Error al abrir el archivo.");
                  return EXIT_FAILURE;
                }
                
                
                
                n = atoi(argv[1]);
                numeros = malloc(n * sizeof(int));
                for(i = 0; i < n; i++){
                  *(numeros + i) = i + 1;
                  fwrite(numeros + i, sizeof(int), 1, salida); 
                }
                
                fclose(salida);
                salida = fopen(argv[2], "r");
                while(fread(&numero, sizeof(int), 1, salida) > 0){
                  printf("numero: %d\n",numero);
                }
         
              
                
     
        /*AQUI VA TU CODIGO*/
                                
                                //******************************************************************            
                                //Evaluar los tiempos de ejecuci�n 
                                //******************************************************************
                                uswtime(&utime1, &stime1, &wtime1);
                                //C�lculo del tiempo de ejecuci�n del programa
                                printf("\n");
                                printf("real (Tiempo total)  %.13f s\n",  wtime1 - wtime0);
                                printf("user (Tiempo de procesamiento en CPU) %.13f s\n",  utime1 - utime0);
                                printf("sys (Tiempo en acciones de E/S)  %.13f s\n",  stime1 - stime0);
                                printf("CPU/Wall   %.13f %% \n",100.0 * (utime1 - utime0 + stime1 - stime0) / (wtime1 - wtime0));
                                printf("\n");
                                //******************************************************************


}